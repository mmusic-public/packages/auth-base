<?php

if (!function_exists('userId')) {
    function userId(): ?string
    {
        return  auth()->id();
    }
}

if (!function_exists('user')) {
    function user(): mixed
    {
        /**
         * Stored at SESSION
         * @see \Mmusic\AuthBase\Auth\Authenticate
         */
        return auth()->user();
    }
}
