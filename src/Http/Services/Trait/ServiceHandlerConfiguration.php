<?php

namespace Mmusic\AuthBase\Http\Services\Trait;

use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

trait ServiceHandlerConfiguration
{
    /**
     * if $defaultTokenExpiracy instance of int must be seconds
     * @var Carbon|int $defaultTokenExpiracy 
     */
    public static function handleServiceConfiguration(string $serviceName): self
    {
        static::guard($serviceName);

        return new static(
            $serviceName,
            config('services.' . $serviceName . '.authentication') == 'dynamic',
            config('services.' . $serviceName . '.base_url'),
            config('services.' . $serviceName . '.prefix'),
            static::buildToken($serviceName)
        );
    }


    public static function buildToken(string $serviceName): ?string
    {
        $credentialType = config('services.' . $serviceName . '.authentication');

        if ($credentialType == 'dynamic') {
            static::configureBearerAuthorizationServiceHandler($serviceName);
        }

        if ($credentialType == 'static') {
            return static::configureStaticServiceHandler($serviceName);
        }

        return null;
    }

    public static function guard(string $serviceName, bool $shouldAbort = true): string
    {
        abort_if(is_null($serviceName), 400, 'Service name cannot be null');

        return static::aborter(
            $serviceName,
            ['base_url', 'prefix'],
            (config('services.' . $serviceName . '.authentication') == 'dynamic'
                ? ['user.name', 'user.pass', 'user.api']
                : ['token']
            ),
            $shouldAbort
        );
    }

    private static function configureStaticServiceHandler(string $serviceName): ?string
    {
        return config('services.' . $serviceName . '.token');
    }

    private static function configureBearerAuthorizationServiceHandler(string $serviceName): string
    {
        return static::getToken($serviceName)
            ?: static::createBearerToken($serviceName);
    }

    private static function aborter(string $serviceName, array $conditions = [], array $secrets = [], bool $shouldAbort): string
    {
        $abortings = [];

        if (config('services.' . $serviceName . '.authentication') !== 'none') {
            $conditions = array_merge($conditions, $secrets);
        }

        foreach ($conditions as $condition) {
            if (is_null(config('services.' . $serviceName . '.' . $condition))) {
                $abortings[] = $condition;
            }
        }

        if ($shouldAbort) {
            abort_if(count($abortings), 400, 'Configuration missing for ' . $serviceName . ' service: ' . (implode(',', $abortings)) . '| Configure then @ ./config/services.php');
        }

        return implode(',', $abortings);
    }

    private static function createBearerToken(string $serviceName): string
    {
        $response = Http::withBasicAuth(config('services.' . $serviceName . '.user.name'), config('services.' . $serviceName . '.user.pass'))
            ->post(static::getComputedAction($serviceName))
            ->throw()
            ->json();

        return static::setToken($serviceName, $response);
    }

    public static function getComputedAction(string $serviceName): string
    {
        $baseUrl = config('services.' . $serviceName . '.base_url');
        $prefix = config('services.' . $serviceName . '.prefix');

        $host = str_ends_with($baseUrl, '/') ? substr_replace($baseUrl, '', -1) : $baseUrl;
        $pathPrefix = str_ends_with($prefix, '/') ? substr_replace($prefix, '', -1) : $prefix;
        $action = config('services.' . $serviceName . '.user.api');

        return $host
            . (str_starts_with($pathPrefix, '/',) ? $pathPrefix : '/' . $pathPrefix)
            . (str_starts_with($action, '/',) ? $action : '/' . $action);
    }

    public static function cacheKeyConst(string $serviceName): string
    {
        return str_replace('{ServiceName}', $serviceName, 'services:{ServiceName}:authentication');
    }

    public static function getToken($serviceName): ?string
    {
        return Cache::get(static::cacheKeyConst($serviceName));
    }

    public static function setToken(string $serviceName, array $response): string
    {
        $expireOption = config('services.' . $serviceName . '.user.access_token.expires', [
            'type' => 'static',
            'key' => 'expire_date',
            'default' => [
                'unit' => 'second',
                'value' => 30 * 60,
            ],
        ]);

        Cache::put(
            static::cacheKeyConst($serviceName),
            config('services.' . $serviceName . '.user.access_token.type', 'Bearer ') . $response[config('services.' . $serviceName . '.user.access_token.key', 'auth_token')],
            $expireOption['type'] == 'dymanic'
                ? Carbon::parse($response[$expireOption['key']])->subMinutes(1)
                : Carbon::now()->add($expireOption['default']['unit'], $expireOption['default']['value'])
        );

        return config('services.' . $serviceName . '.user.access_token.type', 'Bearer ')
            . $response[config('services.' . $serviceName . '.user.access_token.key', 'auth_token')];
    }
}
