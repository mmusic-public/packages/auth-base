<?php

namespace Mmusic\AuthBase\Http\Services;

use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Mmusic\AuthBase\Http\Services\Trait\ServiceHandlerConfiguration;

/**
 * @method  static make(string $serviceName): self                                    // create an instance
 *
 * Setting Request Method
 * @method  asPost(): self                                                            // build request method as post
 * @method  asPut(): self                                                             // build request method as put
 * @method  asDelete(): self                                                          // build request method as delete
 * @method  setMethod(): self                                                         // can dynamically set method
 *
 * Build Request
 * @method  setHeader(string $key, string $value): self                               // can merge header to [$key => $value]
 * @method  setParams(array $params): self                                            // build request body with given params <array>
 * @method  changeHeaderAuthKeyName(string $defaultHeaderkey = 'Auth-Token'): self    // can change header security key with given string
 * 
 * Response Tools
 * @method  sendRequest(): self                                                       // Send request with given action params
 * @method  isSuccess(): bool                                                         // Is sended Request response code is lower than 400
 * @method  getResponse(?string $pluckKey): mixed                                     // get response|pluck data from repsonse by given pluckKey|if not given retrieve full response
 * @method  getException(): \Throwable                                                // get Exception without throwing
 * 
 * DEBUGGING Tools
 * @method  dumpWhenFailed(string $echoTextWhenFailed): self                          // Dump on local environment when request is failed
 * @method  dumpBeforeTheRequest(): self                                              // Dump builded request before send, Also works on only local
 * @method  getSettings(): string                                                     // get environments anytime as json encoded string
 */
class ServiceHttpHandler
{
    use ServiceHandlerConfiguration;

    private ?string $baseUrl = null;
    private ?string $token = null;
    private ?string $pathPrefix = null;
    private string $method = 'get';
    private string $action = '';
    private array $headers = [];
    private array $params = [];

    private mixed $response;
    private bool $isFailed = false;
    private string $dumpOnFailed = '';
    private string $headerSecurityKey = 'Auth-Token';
    private bool $dumpBeforeSend = false;

    private ?string $serviceName = null;
    private ?string $authenticationApi = null;

    private ?\Throwable $exception = null;

    private bool $checkSecrets = true;
    private bool $shouldUseAuthentication = false;

    public function __construct(
        string $serviceName,
        bool $shouldUseAuthentication,
        string $baseUrl,
        string $pathPrefix,
        ?string $token,
    ) {
        $this->serviceName = $serviceName;
        $this->shouldUseAuthentication = $shouldUseAuthentication;
        $this->baseUrl = $baseUrl;
        $this->pathPrefix = $pathPrefix;
        $this->token = $token;

        if ($shouldUseAuthentication) {
            $this->headerSecurityKey = 'Authorization';
        }
    }

    public function withOutConfigs(): self
    {
        $this->checkSecrets = false;

        return $this;
    }

    /**
     * @var string $serviceName
     * @var ?bool $shouldUseAuthentication (default: false)
     * @var Carbon $defaultTokenExpiracy (default: null)
     * @var int $defaultTokenExpiracy (seconds, default: null)
     */
    public static function make(string $serviceName): self
    {
        return static::handleServiceConfiguration($serviceName);
    }

    public function asPost(): self
    {
        $this->method = 'post';

        return $this;
    }

    public function asPut(): self
    {
        $this->method = 'put';

        return $this;
    }

    public function asDelete(): self
    {
        $this->method = 'delete';

        return $this;
    }

    public function setHeader(string $key, mixed $value): self
    {
        $this->headers[$key] = $value;

        return $this;
    }

    public function changeHeaderAuthKeyName(string $keyName = 'Auth-Token'): self
    {
        $this->headerSecurityKey = $keyName;
        $this->headers[$keyName] = $this->token;

        return $this;
    }

    public function setParams(array $params): self
    {
        $this->params = $params;

        return $this;
    }

    public function dumpWhenFailed(string $echoTextWhenFailed): self
    {
        $this->dumpOnFailed = $echoTextWhenFailed;

        return $this;
    }

    public function setMethod(string $method = 'get'): self
    {
        $this->method = $method;

        return $this;
    }

    public function dumpBeforeTheRequest(): self
    {
        $this->dumpBeforeSend = true;

        return $this;
    }

    public function setAction(string $action): self
    {
        $host = str_ends_with($this->baseUrl, '/') ? substr_replace($this->baseUrl, '', -1) : $this->baseUrl;
        $pathPrefix = str_ends_with($this->pathPrefix, '/') ? substr_replace($this->pathPrefix, '', -1) : $this->pathPrefix;

        $this->action = $host
            . (str_starts_with($pathPrefix, '/',) ? $pathPrefix : '/' . $pathPrefix)
            . (str_starts_with($action, '/',) ? $action : '/' . $action);

        return $this;
    }

    private function getAction(string $action): string
    {
        $host = str_ends_with($this->baseUrl, '/') ? substr_replace($this->baseUrl, '', -1) : $this->baseUrl;
        $pathPrefix = str_ends_with($this->pathPrefix, '/') ? substr_replace($this->pathPrefix, '', -1) : $this->pathPrefix;

        return $host
            . (str_starts_with($pathPrefix, '/',) ? $pathPrefix : '/' . $pathPrefix)
            . (str_starts_with($action, '/',) ? $action : '/' . $action);
    }

    public function sendRequest(): self
    {
        if ($this->checkSecrets) {
            static::guard($this->serviceName);

            $this->headers = array_merge($this->headers, [
                $this->headerSecurityKey => $this->token
            ]);
        }

        if ($this->dumpBeforeSend && app()->environment() == 'local') {
            dd(
                'reason:  Dumping before the request',
                'method:  ' . $this->method,
                'headers: ' . json_encode($this->headers),
                'action:  ' . $this->action,
                'params:  ' . json_encode($this->params),
            );
        }

        try {
            $this->isFailed = false;
            $this->response = Http::acceptJson()
                ->withHeaders($this->headers)
                ->{$this->method}($this->action, $this->params)
                ->throw()
                ->json();
        } catch (\Throwable $e) {
            if (app()->environment() == 'local' && $this->dumpOnFailed) {
                dd(
                    'dumpingText: ' . $this->dumpOnFailed,
                    'reason:  Dumping because of exception catched',
                    'message: ' . $e->getMessage(),
                    'headers: ' . json_encode($this->headers),
                    'method:  ' . $this->method,
                    'action:  ' . $this->action,
                    'body:    ' . json_encode($this->params),
                );
            }

            $this->isFailed = true;
            $this->exception = $e;
        }

        return $this;
    }

    public function getResponse(string $pluckKey = null): mixed
    {
        if ($this->isFailed) {
            throw $this->exception;
        }

        return $pluckKey && isset($this->response[$pluckKey])
            ? $this->response[$pluckKey]
            : $this->response;
    }

    public function getException(): \Throwable
    {
        return $this->exception;
    }

    public function isSuccess(): bool
    {
        return !$this->isFailed;
    }

    public function getSettings(): string
    {
        if ($this->checkSecrets) {
            $guardedMissing = static::guard($this->serviceName, false);

            $this->headers = array_merge($this->headers, [
                $this->headerSecurityKey => $this->token
            ]);
        }

        return json_encode([
            'missingConfigs' => $guardedMissing ?? [],
            'headers' => $this->headers,
            'baseUrl' => $this->baseUrl,
            'token' => $this->token,
            'pathPrefix' => $this->pathPrefix,
            'method' => $this->method,
            'action' => $this->action,
            'exception_message' => $this->exception?->getMessage(),
        ]);
    }
}
