<?php

namespace Mmusic\AuthBase\Http\Middleware;

use Closure;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class OpenAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Closure(Request): (Response|RedirectResponse) $next
     * @throws Exception
     */
    public function handle(Request $request, Closure $next, ...$guards): Response|RedirectResponse|JsonResponse
    {
        $guard = implode(',', $guards);

        abort_if(count($guards) > 1, 500, 'AuthToken Middleware doesn\'t support multiple guards');

        $abortingMessage = config('app.tokens.' . $guard . '.message');

        $token = $request->header('Auth-Token');

        abort_if(is_null($token), 400, $abortingMessage ?: '26, Bad Request');

        abort_if($token != $this->getToken($guard), 400, $abortingMessage ?: '28, Bad Request' . (app()->environment() == 'production'  ? '' : '|Required: ' . $this->getToken() . '|Requested:' . $token));

        return $next($request);
    }

    private function getToken(string $guard = null): ?string
    {
        return $guard
            ? config('app.tokens.' . $guard . '.credential')
            : config('app.service_token');
    }
}
