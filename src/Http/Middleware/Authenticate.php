<?php

namespace Mmusic\AuthBase\Http\Middleware;

use Mmusic\AuthBase\Auth\User;
use Closure;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param Closure(Request): (Response|RedirectResponse) $next
     * @throws Exception
     */
    public function handle(Request $request, Closure $next, ...$guards): Response|RedirectResponse|JsonResponse
    {
        $user = $this->getUser($request);

        abort_if(in_array('guest', $guards) && $user, 403, 'Must be guest.');
        abort_if(in_array('guard', $guards) && is_null($user), 401, 'Unauthorized.');
 
        return $next($request);
    }

    private function getUser(Request $request): ?User
    {
        if ($request->header('User-Id')) {
            $attributes = $request->header('User') ?
                json_decode($request->header('User'), true) :
                ['id' => $request->header('User-Id')];

            return User::make($attributes);
        }

        return null;
    }
}
