<?php

namespace Mmusic\AuthBase\Auth;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class AuthServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->registerPolicies();

        Auth::viaRequest('ingress-headers', function (Request $request) {
            if ($request->header('User-Id')) {
                $attributes = $request->header('user') ?
                    json_decode($request->header('user'), true) :
                    ['id' => $request->header('User-Id')];

                return User::make($attributes);
            }
        });

        Auth::viaRequest('redis-headers', function (Request $request) {
            if ($user = $this->getUserData($request->bearerToken())) {
                return User::make($user);
            }
        });
    }

    /**
     * @throws Exception
     */
    private function getUserData(string $accessToken = null): ?array
    {
        if (!str_starts_with(app()->version(), '11')) {
            Cache::setPrefix('');
        } else if (Cache::getPrefix()) {
            throw new \Exception('Prefix found please change it');
        }


        return $accessToken ? Cache::get('auth:user:' . $accessToken) : null;
    }
}
