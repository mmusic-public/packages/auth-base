<?php


namespace Mmusic\AuthBase\Auth;

use Illuminate\Auth\GenericUser;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Support\Arrayable;

class User extends GenericUser implements Arrayable, Authenticatable
{
    public static function make(array $attributes): self
    {
        if (isset($attributes['id'])) {
            return new static($attributes);
        }

        return new static(array_merge($attributes, [
            'id' => $attributes['user_id']
        ]));
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return $this->attributes;
    }

    public function getIdAttribute(): ?string
    {
        return $this->attributes['id'] ?? null;
    }
}
