<?php


namespace Mmusic\AuthBase\Auth;

use Closure;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$guards)
    {
        abort_if(in_array('guard', $guards) && auth()->guard(config('auth.defaults.guard', 'ingress'))->guest(), 401, 'Unauthenticated.');
        abort_if(in_array('guest', $guards) && auth()->guard(config('auth.defaults.guard', 'ingress'))->check(), 403, 'Must be guest.');

        return $next($request);
    }
}
