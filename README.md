<p align="center"><a href="https://www.mmusic.mn" target="_blank"><img src="https://d15xdsbb8need2.cloudfront.net/images/logo/mmusic_main_white_transparent_logo.png" width="300"></a></p>

## Ашиглах заавар

Зөвхөн <span style="color: green">Laravel framework</span> дээр ашиглагдана.

#### 1. <span style="color: green">composer.json</span> дээр доорхи кодийг нэмнэ үү

```
...
"repositories": [
    {
        "type": "git",
        "url": "git@gitlab.com:mmusic-public/packages/auth-base.git"
    }
],
....
"require": {
    ...
    "mmusic/auth-base": "^0.4.3",
    ...
},
...
```

Нэмсний дараа <span style="color: green">composer install</span> эcвэл <span style="color: green">composer update</span> комманд ажиллуулна уу

#### 2. a. <span style="color: green">Http</span> дотор байрлах <span style="color: green">Kernel.php</span> дээр доорхи кодийг нэмнэ үү <br> <span style="color: red">laravel 10 болон түүнээс өмнө. </span>

```
...
protected $routeMiddleware = [
    ...
   'auth' => \Mmusic\AuthBase\Auth\Authenticate::class,
   ...
];
...
```

#### 2. b. <span style="color: green">Http</span> дотор байрлах <span style="color: green">Kernel.php</span> дээр доорхи кодийг нэмнэ үү

```
...
protected $routeMiddleware = [
   ...
   'open-api' => \Mmusic\AuthBase\Http\Middleware\OpenAuthMiddleware::class,
   ...
];
...
```

#### 2. a. <span style="color: green">Http</span> дотор байрлах <span style="color: green">Kernel.php</span> дээр доорхи кодийг нэмнэ үү <br> <span style="color: red">laravel 11 болон түүнээс хойш. </span>

```
...
    ->withMiddleware(function (Middleware $middleware) {
        $middleware->alias([
        'auth' => \Mmusic\AuthBase\Auth\Authenticate::class,
        ]);
    });
...
```

#### 2. b. <span style="color: green">Http</span> дотор байрлах <span style="color: green">Kernel.php</span> дээр доорхи кодийг нэмнэ үү

```
...
    ->withMiddleware(function (Middleware $middleware) {
        $middleware->alias([
            'open-api' => \Mmusic\AuthBase\Http\Middleware\OpenAuthMiddleware::class,
        ]);
    })
...
```

#### Хэрэглээ

##### bootstrap/app.php || App/Providers/RouteServiceProvider.php файлуудад ашиглахдаа.

```
...
Route::middleware('open-api:{{SERVICE_NAME}}');
...
```

##### Controller -ийн \_construct дотор ашиглахдаа

```
...

public function __construct()
{
    $this->middleware('open-api:{{SERVICE_NAME}}');
}
...
```

##### NOTE:

```
 SERVICE_NAME буюу guard бичихгүй байж болно.
    SERVICE_NAME guard агуулаагүй үед default
    config('app.service_token')
    credential ашиглана.
    Тохируулах зааврыг 4.b -ээс харна уу.
```

- <span style="color: yellow">auth</span> middleware-ийг дараах байдлаар ашиглаж болно

- <span style="color: green">auth</span> #нэвтэрсэн, нэвтэрээгүй аль нь ч байж болно

#### 3. <span style="color: green">config</span> дотор байрлах <span style="color: green">auth.php</span> дээр доорхи кодийг нэмэхэд хангалттай.

```
...

'defaults' => [
    'guard' => 'ingress',
],
...
'guards' => [
    ...
    'ingress' => [
        'driver' => 'ingress-headers',
    ]
    ...
],

...
```

#### 4.b. <span style="color: green">config</span> дотор байрлах <span style="color: green">./config/app.php</span> дээр доорхи кодийг нэмж тохируулна уу

```
...

'service_token' => env('SERVICE_TOKEN'),

'tokens' => [
    '{{SERVICE_NAME}}' => [
        'credential' => env('{{SERVICE_TOKEN_CREDENTIAL}}', env('SERVICE_TOKEN')),
        'message' => 'Invalid Credential'
    ],
],

...
```

"Invalid Credential" нь тухайн харьцаж байгаа сервист мэссеж-ийг customize хийх боломжтой болгоно.

#### 5. <span style="color: green; font-size: 20px;">ServiceHttpHandler</span> helper class -ийн ашиглах заавар

##### 5a.

```
use Mmusic\AuthBase\Http\Services\ServiceHttpHandler;

/**
 * @method  static make(string $serviceName): self                                    // create an instance
 *
 * Setting Request Method
 * @method  asPost(): self                                                            // build request method as post
 * @method  asPut(): self                                                             // build request method as put
 * @method  asDelete(): self                                                          // build request method as delete
 * @method  setMethod(): self                                                         // can dynamically set method
 *
 * Build Request
 * @method  setHeader(string $key, string $value): self                               // can merge header to [$key => $value]
 * @method  setParams(array $params): self                                            // build request body with given params <array>
 * @method  changeHeaderAuthKeyName(string $defaultHeaderkey = 'Auth-Token'): self    // can change header security key with given string
 *
 * Response Tools
 * @method  sendRequest(): self                                                       // Send request with given action params
 * @method  isSuccess(): bool                                                         // Is sended Request response code is lower than 400
 * @method  getResponse(?string $pluckKey): mixed                                     // get response|pluck data from repsonse by given pluckKey|if not given retrieve full response
 * @method  getException(): \Throwable                                                // get Exception without throwing
 *
 * DEBUGGING Tools
 * @method  dumpWhenFailed(): self                                                    // Dump on local environment when request is failed
 * @method  dumpBeforeTheRequest(): self                                              // Dump builded request before send, Also works on only local
 * @method  getSettings(): string                                                     // get environments anytime as json encoded string
 */

Example:
{
    ...
    $handler = ServiceHttpHandler::make('music_logs')
        ->asPost()
        ->setHeader('Accept', 'application/json')
        ->setHeader('Device', 'android')
        ->setAction('listens')
        ->setParams([
            'song_id' => 1000,
            'user_id' => 2000,
        ])
        ->setParams([
            'subscription_id' => 100000
        ])
        ->dumpWhenFailed()
        ->sendRequest();

    if ($handler->isSuccess()) {
        $response = $handler->getResponse('data');
    } else {
        $exception = $handler->getException();
    }
    ...
}

```

#### 6. <span style="color: green; font-size: 20px;">ServiceHttpHandler</span> helper class -ийн тохируулах заавар

##### 6a. Auth-Token ашиглах заавар <span style="color: green">config</span> folder доторх байрлах <span style="color: green">services.php</span> дээр доорхи кодийг нэмж тохируулна уу

```
    ...
    '{{SERVICE_NAME}}' => [
        'base_url' => 'https://{serviceUrl}, example: "https://api.mbook.mn"',
        'prefix' => '{service base prefix, example: "/mbook/open" }'
        'token' => 'Service Static Token Credential',
        'authentication' => 'static',
    ]
    ...
```

##### 6b. Bearer Token ашиглах заавар <span style="color: green">config</span> folder доторх байрлах <span style="color: green">services.php</span> дээр доорхи кодийг нэмж тохируулна уу

```
    ...
    '{{SERVICE_NAME}}' => [
        'base_url' => 'https://{serviceUrl}, example: "https://api.mbook.mn"',
        'prefix' => '{service base prefix example: "/mbook/open" }'
        'authentication' => 'dynamic',
        'user' => [
            'name' => "{BasicAuthUserName}",
            'pass' => "{BasicAuthUserPassword}",
            'api' => "{Service Login Api, example: "login"}",
            'access_token' => [
                'key' => {Basic auth ашиглан нэвтрэх үед ирэх $response -ын хандах token байрлах key, example: "auth_token" },
                'type' => {Basic auth нэвтрэлтэд ашиглахад дараагийн хүсэлтүүдэд ашиглагдах токены төрөл, жишээ: "Bearer "},
                'expires' => [
                    'type' => {'dymanic|static'},
                    'key' => {'expire_date', "Note": "dynamic үед л хэрэглэнэ."},
                    'default' => [ // Note: static үед ашиглана.
                        'unit' => 'second', // Токеныг redis -т хадгалах хугацааны төрөл, default: second
                        'value' => 30 * 60 // Токеныг redis -т хадгалах хугацаа
                    ]
                ],
            ],
        ],
    ]
    ...

    NOTE: Хэрэв зөв тохируулаад Authentication Token авж хүсэлт цааш дамжсан бол Cache -д "{{CACHE_PREFIX}}services:{{SERVICE_NAME}}:authentication" гэсэн түлхүүрт хадгалагдсан байгаа.

```

##### 6c. Token Header ашиглахгүй байх заавар <span style="color: green">config</span> folder доторх байрлах <span style="color: green">services.php</span> дээр доорхи кодийг тохируулна уу

```
    ...
    '{{SERVICE_NAME}}' => [
        'base_url' => 'https://{serviceUrl}, example: "https://api.mbook.mn"',
        'prefix' => '{service base prefix example: "/mbook/open" }'
        'authentication' => 'none',
    ]
    ...

    NOTE: Хэрэв none гэж тохируулсан бол тухайн харьцах сервис нь ямар нэгэн хамгаалалтгүй эсвэл нууцлалт хамгаалалт өөр байдлаар бичигдсэнийг илтгэнэ.

```
